import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const GameSchema = new Schema({
   rounds : [{
     "type" : Schema.Types.ObjectId,
     "ref" : "Round"
   }],
  name : String,
  players : [{
     "type" : Schema.Types.ObjectId,
     "ref" : "Player"
  }],
  hostplayer : {
    "type" : Schema.Types.ObjectId,
    "ref" : "Player"
  },
  shareid : String,
  status : String,
  startingtime : Date
});

export const PlayerSchema = new Schema({
  name : String,
  game : {
    "type" : Schema.Types.ObjectId,
    "ref" : "Game"
  },
  connectionId : String,
  avatar: String,
  points: Number
});

export const RoundSchema = new Schema({
  game : {
    "type" : Schema.Types.ObjectId,
    "ref" : "Game"
  },
  roundnumber : Number,
  moderator :  {
    "type" : Schema.Types.ObjectId,
    "ref" : "Player"
  },
  playeranswers : [{
    "type" : Schema.Types.ObjectId,
    "ref" : "PlayerAnswers"
  }],
  status : String,
  roundstarttime : Date,
  playerchoices:  [{
    "type" : Schema.Types.ObjectId,
    "ref" : "PlayerChoice"
  }],
  roundhistory : {
    "type" : Schema.Types.ObjectId,
    "ref" : "RoundHistory"
  }
});

export const PlayerAnswerSchema = new Schema({
  player : {
    "type" : Schema.Types.ObjectId,
    "ref" : "Player"
  },
  answer : String,
  ismodanswer : Boolean
});

export const PlayerChoiceSchema = new Schema({
  player : {
    "type" : Schema.Types.ObjectId,
    "ref" : "Player"
  },
  choice : {
    "type" : Schema.Types.ObjectId,
    "ref" : "PlayerAnswer"
  }
});

export const RoundHistorySchema = new Schema({
  pointtable : {
    type : Map,
    of: Number
  },
  round : Number
});

// Models
export const GameModel = mongoose.model("Game", GameSchema);
export const PlayerModel = mongoose.model("Player", PlayerSchema);
export const PlayerAnswerModel = mongoose.model("PlayerAnswer", PlayerAnswerSchema);
export const PlayerChoiceModel = mongoose.model("PlayerChoice", PlayerChoiceSchema);
export const RoundModel = mongoose.model("Round", RoundSchema);
export const RoundHistoryModel = mongoose.model("RoundHistory", RoundHistorySchema);